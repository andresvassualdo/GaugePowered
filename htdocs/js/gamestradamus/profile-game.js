define([
    "dojo/_base/array",
    "dojo/_base/event",
    "dojo/on",
    "dojo/query",
    "dojo/dom",
    "dojo/dom-class",
    "atemi/io/jsonrpc",
    "bootstrap/Modal",
    "bootstrap/Tooltip",
    "bootstrap/Popover",
    "gamestradamus/region"
], function(array, event, on, query, dom, domClass, jsonrpc, Modal, Tooltip, Popover, region) {

    var init = function() {
        /* Provide a mechanism for a person that already owns this game to 
           rate the game from the game details interface */
        var rpc = jsonrpc('/jsonrpc');
        var rating = dom.byId('your-rating');
        if (rating) {
            var id = parseInt(rating.getAttribute('data-user-account-game-id'));
            var links = query('a', rating); 
            array.forEach(links, function(link) {
                on(link, 'click', function(evt) {
                    event.stop(evt);
                    var rating_value = parseInt(link.getAttribute('data-rating'));
                    if (isNaN(rating_value)) {
                        return;
                    }

                    rpc.request({
                        method: 'useraccountgame.update',
                        params: [
                            'id=' + id,
                            {rating: rating_value}
                        ]
                    }, function(data) {
                        /* Update the active star states */
                        rating.setAttribute('data-rating', rating_value);
                        query('div', rating).removeClass('active');
                        for (var i = 1; i <= rating_value; ++i) {
                            query('div.rating-' + i, rating).addClass('active');
                        }
                    });
                });
            });
        }

        var container = query('article.game')[0];

        /* Add community value tooltips */
        array.forEach(query('div.community-statistics h2 mark'), function(item) {
            query(item).tooltip({
                container: container,
                trigger: 'hover',
                placement: 'top',
                html: true,
                title: function() {
                    var title = '<p>The value an average member of the Gauge community could get from <strong>' 
                        + item.getAttribute('data-name') + '</strong> at <strong>';
                    if (region['symbol-position'] == 'before') {
                        title += region['symbol'] + region['format-dollars'](item.getAttribute('data-price'));
                    } else {
                        title += region['format-dollars'](item.getAttribute('data-price')) + region['symbol'];
                    }
                    title += '</strong></p>';
                    return title;
                }
            });
        });

        /* Add a tooltip for the similar games */
        array.forEach(query('div.similar-game-list h2 mark'), function(item) {
            query(item).tooltip({
                container: container,
                trigger: 'hover',
                placement: 'top',
                html: true,
                title: '<p>The distribution of time spent playing <strong>' + item.getAttribute('data-name') + '</strong> across all members of the Gauge community. ' +
                    'The median, <strong>' + region['format-hours'](item.getAttribute('data-hours')) + ' hours</strong> is denoted by the dotted line.</p>'
            });
        });

        /* Add watchlist button tooltip */
        query('#on-watchlist').popover({
            container: container,
            trigger: 'manual',
            placement: 'top',
            html: true,
            content:
                '<div id="added-to-watchlist">' +
                '   <p><strong>Added to your Watchlist.</strong></p>' + 
                '   <p>Set notification limits through your Watchlist.</p>' +
                '   <a class="close" title="close">×</a>' +
                '</div>'
        });
        query('#add-to-watchlist').tooltip({
            container: container,
            trigger: 'hover',
            placement: 'top',
            html: true,
            title: '<p><strong>Add to your Watchlist.</strong></p>' + 
                '<p>Your Watchlist can be configured to notify you when the price drops.</p>'
        });

        // Bring the add to watchlist button alive!
        var watch_list_handle = on(dom.byId('add-to-watchlist'), 'click', function(evt) {
            watch_list_handle.remove();
            event.stop(evt);

            var link = evt.currentTarget;
            var game_id = parseInt(link.getAttribute('data-game-id'));
            rpc.request({
                method: 'useraccountgamewatch.create',
                params: ['', {game_id: game_id}]
            }, function(data) {
                query('#on-watchlist').tooltip('destroy');
                domClass.add(dom.byId('add-to-watchlist'), 'hidden');
                domClass.remove(dom.byId('on-watchlist'), 'hidden');
                query('#on-watchlist').popover('show');
                query('#in-library').popover('hide');

                var added_to_watchlist = query('#added-to-watchlist')[0];
                var close = query('.close', added_to_watchlist)[0];

                on(close, 'click', function(evt) {
                    event.stop(evt);
                    query('#on-watchlist').popover('hide');
                });
            });
        }); 

        /* Create tooltip and popup for the add to library page. */
        container = dom.byId('add-to-library-interface');

        query('#in-library').popover({
            container: container,
            trigger: 'manual',
            placement: 'top',
            html: true,
            content:
                '<div id="added-to-library">' +
                '   <p><strong>Added to your Library.</strong></p>' + 
                '   <p>Edit your hours played and cost in your library.</p>' +
                '   <a class="close" title="close">×</a>' +
                '</div>'
        });
        query('#add-to-library, #restore-to-library, a.premium-support').tooltip({
            container: container,
            trigger: 'hover',
            placement: 'top',
            html: true,
            title: '<p><strong>Add to your Library.</strong></p>' + 
                '<p>Include this game in your analytics by adding it to your library.</p>'
        });

        // Bring the add to library button alive!
        var library_handle = query("#add-to-library").on('click', function(evt) {
            library_handle.remove();
            event.stop(evt);

            var link = evt.currentTarget;
            var game_id = parseInt(link.getAttribute('data-game-id'));
            rpc.request({
                method: 'useraccountgame.create',
                params: ['', {game_id: game_id}]
            }, function(data) {
                query('#add-to-library, #restore-to-library').tooltip('destroy');
                domClass.add(dom.byId('add-to-library'), 'hidden');
                domClass.remove(dom.byId('in-library'), 'hidden');
                query('#in-library').popover('show');
                query('#on-watchlist').popover('hide');

                var close = query('.close', dom.byId('added-to-library'))[0];
                on(close, 'click', function(evt) {
                    event.stop(evt);
                    query('#in-library').popover('hide');
                });
            });
        }); 

        // Bring the restore-to-library to life!
        var restore_to_library_handle = query("#restore-to-library").on('click', function(evt) {
            restore_to_library_handle.remove();
            event.stop(evt);

            var link = evt.currentTarget;
            var uag_id = parseInt(link.getAttribute('data-user-account-game-id'));
            rpc.request({
                method: 'useraccountgame.update',
                params: ['id=' + uag_id, {ignore: false}]
            }, function(data) {
                query('#add-to-library, #restore-to-library').tooltip('destroy');
                domClass.add(dom.byId('restore-to-library'), 'hidden');
                domClass.remove(dom.byId('in-library'), 'hidden');
                query('#in-library').popover('show');
                query('#on-watchlist').popover('hide');

                var close = query('.close', dom.byId('added-to-library'))[0];
                on(close, 'click', function(evt) {
                    event.stop(evt);
                    query('#in-library').popover('hide');
                });

                // TODO: Maybe later provide them an edit interface in place.
            });
        }); 

    };
    return init;
});
