define([
    "atemi/io/jsonrpc",
    "atemi/thirdparty/moment",
    "atemi/util/format",
    "atemi/util/TableSort",

    "bootstrap/Tooltip",
    "bootstrap/Modal",

    "dojo/_base/array",
    "dojo/_base/event",
    "dojo/_base/fx",
    "dojo/dom",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/keys",
    "dojo/mouse",
    "dojo/on",
    "dojo/query",

    "gamestradamus/region",

    "dojo/NodeList-dom"
], function(
    jsonrpc,
    moment,
    format,
    TableSort,

    Tooltip,
    Modal,

    array,
    event,
    fx,
    dom,
    domClass,
    domConstruct,
    keys,
    mouse,
    on,
    query,

    region
) {

    var table = dom.byId('library');
    var games = query('tbody tr.game', table);
    var sort_type = 'hours-desc';

    //--------------------------------------------------------------------------
    var _fix_table_stripes = function() {
        var index = 1,
            hours_played = null,
            dollars_paid = null;
        for (var i = 0, len = games.length, tr; i < len; ++i) {
            tr = games[i];
            if (domClass.contains(tr, 'blur') || domClass.contains(tr, 'ignore')) {
                continue;
            }

            // Fix the positioning column.
            tr.cells[0].innerHTML = '<div>' + index + '</div>';
            
            // Fix the table striping.
            if (index % 2) {
                domClass.add(tr, 'odd');
            } else {
                domClass.remove(tr, 'odd');
            }
            index++;
        }
    };

    //--------------------------------------------------------------------------
    var extract_hash_parameters = function(hash) {
        // Note: This method varies from dojo/io-query by always returning values
        //       as an array. This makes the rest of the code in this library much
        //       easier to write. 

        var search = window.location.hash.replace('#', '');
            search = search.split('&');
        var params = {};
        array.forEach(search, function(q) {
            q = q.split('=');
            if (!q[0]) {
                return;
            }
            params[q[0]] = decodeURIComponent(q[1]).split('_');
        });
        return params;
    };

    //--------------------------------------------------------------------------
    var encode_hash_parameters = function(parameters) {
        var hash = '';
        var first = true;
        for (key in parameters) {
            if (!parameters[key]) {
                continue;
            }

            if (first) {
                hash = key + '=';
                first = false;
            } else {
                hash = hash + '&' + key + '=';
            } 

            hash = hash + encodeURIComponent(parameters[key][0]);
            for (var i = 1; i < parameters[key].length; ++i) {
                hash = hash + '_' + encodeURIComponent(parameters[key][i]);
            }
        }
        if (!hash) {
            // Ensure there is at least one parameter on the hash tag to 
            // avoid the browser snapping the scroll to the top for just #
            hash = 'pre=1';
        }

        return hash;
    };

    //--------------------------------------------------------------------------
    var tabulate = function() {
        /**
         * Tabulate the values displayed in the table.
        **/
        var hours_played = 0;
        var dollars_paid = 0;
        var steam_price = 0;
        var rating = 0;

        var counts = {
            total: 0,
            rating: 0,
            cost: 0
        };

        array.forEach(games, function(item) {
            if (domClass.contains(item, 'ignore')) {
                return;
            }
            if (domClass.contains(item, 'blur')) {
                return;
            }
            var value = null;

            counts.total++;
            if (!domClass.contains(item.cells[2], 'uncorrected')) {
                counts.cost++;
            }

            value = region['parse-hours'](item.cells[3].innerHTML);
            if (!isNaN(value)) {
                hours_played = hours_played + value;
            }
            value = region['parse-dollars'](item.cells[2].innerHTML);
            if (!isNaN(value)) {
                dollars_paid = dollars_paid + value;
            }

            value = parseFloat(item.cells[2].getAttribute('data-steam-price'));
            if (!isNaN(value)) {
                steam_price = steam_price + value;
            }

            var value = parseInt(query('div.rating', item.cells[5])[0].getAttribute('data-rating'));
            if (value > 0) {
                rating = rating + value;
                counts.rating++;
            }
        });

        var item = query('tfoot tr', table)[0];
        if (region['symbol-position'] == 'before') {
            item.cells[2].innerHTML = 
                region['symbol'] +
                region['format-dollars'](dollars_paid);
        } else {
            item.cells[2].innerHTML =
                region['format-dollars'](dollars_paid) +
                region['symbol'];
        }

        item.cells[3].innerHTML = region['format-hours'](hours_played);
        if (dollars_paid > 0) {
            item.cells[4].innerHTML = region['format-cost-per-hour'](dollars_paid / hours_played);
        } else {
            item.cells[4].innerHTML = '&mdash;';
        }
        if (counts.rating > 0) {
            item.cells[5].innerHTML = region['format-rating'](rating / counts.rating);
        } else {
            item.cells[5].innerHTML = '&mdash;';
        }

        // Update the global statistics.
        var stat;
        stat = query('div.average-cost-per-hour div.stat')[0];
        if (hours_played > 0) {
            if (Math.round((dollars_paid * 100) / hours_played) >= 100) {
                if (region['symbol-position'] == 'before') {
                    stat.innerHTML =
                        '<span class="unit">' + region['symbol'] + '</span>' +
                        region['format-cost-per-hour'](dollars_paid / hours_played);
                } else {
                    stat.innerHTML =
                        region['format-cost-per-hour'](dollars_paid / hours_played) +
                        '<span class="unit">' + region['symbol'] + '</span>';
                }
            } else {
                stat.innerHTML = ((dollars_paid / hours_played) * 100).toFixed(0) +
                    '<span class="unit">' + region['fraction-symbol'] + '</span>';
            }
            stat.setAttribute('data-stat', (dollars_paid / hours_played).toFixed(4));
        } else {
            stat.innerHTML = '0<span class="unit">' + region['fraction-symbol'] + '</span>';
            stat.setAttribute('data-stat', '0.00');
        }

        stat = query('div.total-cost div.stat')[0];
        stat.innerHTML = region['format-dollars'](dollars_paid);

        var title = 'They\'ve saved <strong>';
        if (region['symbol-position'] == 'before') {
            title += region['symbol'] + region['format-dollars'](steam_price - dollars_paid);
        } else {
            title += region['format-dollars'](steam_price - dollars_paid) + region['symbol'];
        }
        title += '</strong>';
        query('div.total-cost').tooltip('destroy');
        query('div.total-cost').tooltip({
            trigger: 'hover',
            position: 'top',
            html: true,
            title: title
        });

        stat = query('div.total-playtime div.stat')[0];
        stat.innerHTML = region['format-hours'](hours_played);

        stat = query('div.average-cost div.stat')[0];
        if (counts.total > 0) {
            stat.innerHTML = region['format-dollars'](dollars_paid / counts.total);
        } else {
            stat.innerHTML = '0.00';
        }

        stat = query('div.average-playtime div.stat')[0];
        if (counts.total > 0) {
            stat.innerHTML = region['format-hours'](hours_played / counts.total);
        } else {
            stat.innerHTML = '0.0';
        }

        // Update the Cost completion progress bar.
        var cost_percentage = ((counts.cost / counts.total) * 100);
        query('span', 'progress-bar').forEach(function(span) {
            if (cost_percentage > 0) {
                span.innerHTML = '<strong>' + cost_percentage.toFixed(1) + 
                    '%</strong> (' + counts.cost + ' of ' + counts.total + ' games) corrected';
            } else {
                span.innerHTML = '';
            }
        });

        var progress = query('.progress', 'progress-bar')[0];
        progress.style.width = cost_percentage.toFixed(1) + '%';
        if (counts.cost == counts.total) {
            domClass.add(dom.byId('progress-bar'), 'complete');
        } else {
            domClass.remove(dom.byId('progress-bar'), 'complete');
        }

        // Show the help tooltip when a profile has zero corrected costs.
        if (counts.cost < 1 && counts.total > 0) {
            query('a', 'cost-column-header').tooltip('show');
        } else {
            query('a', 'cost-column-header').tooltip('hide');
        }
    };

    //--------------------------------------------------------------------------
    var setup_table_sorting = function() {
        /**
         * Setup the table sorting.
        **/
        var cost_per_hour_anchor = query("#cost-per-hour-column-header a")[0];
        new TableSort(
            table, [0, 0], [
                TableSort.textContent,
                TableSort.textContent,
                function(td) {
                    if (td) {
                        return region['parse-dollars'](td.textContent);
                    } else {
                        return 0.00;
                    }
                },
                function(td) {
                    if (td) {
                        return region['parse-hours'](td.textContent);
                    } else {
                        return 0.00;
                    }
                },
                function(td) {
                    if (td) {
                        var order = domClass.contains(
                            cost_per_hour_anchor, 'sort-descending'
                        );
                        var value = region['parse-hours'](td.textContent);
                        if (isNaN(value)) {
                            if (!order) {
                                return Infinity;
                            } else {
                                return -Infinity;
                            }
                        }
                        return value;
                    } else {
                        return 0;
                    }
                },
                function(td) {
                    if (td) {
                        var rating = query('div.rating', td)[0];
                        var cost_per_hour = query('span.cost-per-hour', td.parentNode)[0];
                            cost_per_hour = region['parse-hours'](cost_per_hour.textContent);
                        if (isNaN(cost_per_hour)) {
                            cost_per_hour = Infinity;
                        } 
                        return [
                            parseInt(rating.getAttribute('data-rating')),
                            -cost_per_hour
                        ];
                    } else {
                        return [0, 0];
                    }
                }
            ],
            function(data) {
                // Refresh the game list with the new sort ordering.
                games = query('tr.game', table);

                // Make sure that everything is striped properly.
                _fix_table_stripes();

                // Update the window.location.hash to include the table sorting.
                var sort, params;
                array.some([
                    ['name', 'ASC'],
                    ['name', 'DESC'],
                    ['cost', 'ASC'],
                    ['cost', 'DESC'],
                    ['hours', 'ASC'],
                    ['hours', 'DESC'],
                    ['rating', 'ASC'],
                    ['rating', 'DESC'],
                    ['cost-per-hour', 'ASC'],
                    ['cost-per-hour', 'DESC']
                ], function(item) {
                    if (item[1] == 'ASC') {
                        if (domClass.contains(query('>a', item[0] + '-column-header')[0], 'sort-ascending')) {
                            sort = item;
                            return true;
                        }
                    } else if (item[1] == 'DESC') {
                        if (domClass.contains(query('>a', item[0] + '-column-header')[0], 'sort-descending')) {
                            sort = item;
                            return true;
                        }
                    }
                });

                params = extract_hash_parameters(window.location.hash);
                params['sort'] = sort;
                window.location.hash = encode_hash_parameters(params);
            }
        ); 
    };

    //---------------------------------------------------------------------------
    var _filter_library = function() {

        // Which filters are current in effect?
        var tag_filter_list = [];
        var untag_filter_list = [];
        var genre_filter_list = [];
        var finished_filter_list = [];
        var hours_filter_list = [];
        var cost_filter_list = [];
        var apply_rating_filter = false;
        var rating_filter_map = {};
        var applied_filters = false;
        query('.filter-list >.filter', 'filter-list').forEach(function(filter) {
            if (domClass.contains(filter, 'hidden')) {
                return;
            }
            applied_filters = true;
            
            if (filter.getAttribute('data-tag-name')) {
                tag_filter_list.push(filter.getAttribute('data-tag-name'));

            } else if (filter.getAttribute('data-untag-name')) {
                untag_filter_list.push(
                    filter.getAttribute('data-untag-name').split(' / ')[0]
                );

            } else if (filter.getAttribute('data-genre')) {
                genre_filter_list.push(filter.getAttribute('data-genre'));

            } else if (filter.getAttribute('data-rating')) {
                rating_filter_map[filter.getAttribute('data-rating')] = true;
                apply_rating_filter = true;

            } else if (filter.getAttribute('data-finished')) {
                finished_filter_list.push(filter.getAttribute('data-finished'));

            } else if (filter.getAttribute('data-cost')) {
                cost_filter_list.push(filter.getAttribute('data-cost'));   

            } else if (filter.getAttribute('data-hours')) {
                hours_filter_list.push(filter.getAttribute('data-hours'));
            }
        }); 

        // Update window.location.hash
        var params = extract_hash_parameters(window.location.hash);

        var composite_tag_filter_list = [];
        array.forEach(tag_filter_list, function(filter) {
            composite_tag_filter_list.push(filter);
        });
        array.forEach(untag_filter_list, function(filter) {
            composite_tag_filter_list.push(filter + ' / No Tag');
        });
        if (composite_tag_filter_list.length) {
            params['tag'] = composite_tag_filter_list;
        } else {
            params['tag'] = undefined;
        }

        if (genre_filter_list.length) {
            params['genre'] = genre_filter_list;
        } else {
            params['genre'] = undefined;
        }

        if (cost_filter_list.length) {
            params['cost'] = cost_filter_list;
        } else {
            params['cost'] = undefined;
        }

        if (hours_filter_list.length) {
            params['hours'] = hours_filter_list;
        } else {
            params['hours'] = undefined;
        }

        if (finished_filter_list.length) {
            params['finished'] = finished_filter_list;
        } else {
            params['finished'] = undefined;
        }

        if (apply_rating_filter) {
            var enjoyment = [];
            for (key in rating_filter_map) {
                enjoyment.push([key]);
            }
            params['enjoyment'] = enjoyment;
        } else {
            params['enjoyment'] = undefined;
        }
        window.location.hash = encode_hash_parameters(params);

        // Open and close the filter interface.
        var rollup = query('.rollup', 'filter-list')[0];
        if (applied_filters && !domClass.contains(rollup, 'open')) {
            domClass.add(rollup, 'open');
            fx.animateProperty({
                node: rollup,
                properties: {
                    height: {start: 0, end: 34},
                    paddingBottom: {start: 0, end: 10}
                },
                onEnd: function() {
                    rollup.style.height = 'auto';
                }
            }).play(); 
        } else if(!applied_filters && domClass.contains(rollup, 'open')) {
            fx.animateProperty({
                node: rollup,
                properties: {
                    height: {start: rollup.clientHeight - 10, end: 0},
                    paddingBottom: {start: 10, end: 0}
                },
                onEnd: function() {
                    domClass.remove(rollup, 'open');
                }
            }).play(); 
        }

        var finished_map = {
            "Yes": "Yes",
            "No": "No"
        };
        var hours_map = {
            "0-1": [0, 1],
            "1-8": [1, 8],
            "8-20": [8, 20],
            "20-40": [20, 40],
            "40-100000": [40, 100000]
        };
        var cost_map = {
            "0-5": [0, 5],
            "5-10": [5, 10],
            "10-20": [10, 20],
            "20-40": [20, 40],
            "40-10000": [40, 10000],

            "0-150": [0, 150],
            "150-300": [150, 300],
            "300-500": [300, 500],
            "500-700": [500, 700],
            "700-10000": [700, 10000]
        };

        var games = table.tBodies[0].rows;
        var count = games.length;
        var tr, group_map, tag_map, genres, filtered;
        for (var i = 0, len = games.length; i < len; ++i) {
            tr = games[i];
            domClass.remove(tr, 'blur');

            if (apply_rating_filter) {
                if (!rating_filter_map[query('div.rating', tr.cells[5])[0].getAttribute('data-rating')]) {
                    domClass.add(tr, 'blur');
                    count--;
                    continue;
                }
            }

            if (finished_filter_list.length) {
                if (tr.cells[6].textContent != finished_map[finished_filter_list[0]]) {
                    domClass.add(tr, 'blur');
                    count--;
                    continue;
                }
            }

            if (cost_filter_list.length) {
                var cost = region['parse-dollars'](tr.cells[2].textContent);
                if (isNaN(cost)) {
                    domClass.add(tr, 'blur');
                    count--;
                    continue;
                }

                filtered = true;
                for (var j = 0, l = cost_filter_list.length; j < l; ++j) {
                    if (
                        (cost_map[cost_filter_list[j]][0] <= cost) &&
                        (cost_map[cost_filter_list[j]][1] > cost)
                    ) {
                        filtered = false;
                        break;
                    }
                } 

                if (filtered) {
                    domClass.add(tr, 'blur');
                    count--;
                    continue;
                }
            }

            if (hours_filter_list.length) {
                var hours = region['parse-hours'](tr.cells[3].textContent);
                if (isNaN(hours)) {
                    domClass.add(tr, 'blur');
                    count--;
                    continue;
                }

                filtered = true;
                for (var j = 0, l = hours_filter_list.length; j < l; ++j) {
                    if (
                        (hours_map[hours_filter_list[j]][0] <= hours) &&
                        (hours_map[hours_filter_list[j]][1] > hours)
                    ) {
                        filtered = false;
                        break;
                    }
                }

                if (filtered) {
                    domClass.add(tr, 'blur');
                    count--;
                    continue;
                }
            }

            if (genre_filter_list.length) {
                genres = query('.genre', tr.cells[1])[0];
                if (!genres) {
                    domClass.add(tr, 'blur');
                    count--;
                    continue;

                } else {
                    filtered = false;
                    for (var j = 0, l = genre_filter_list.length; j < l; ++j) {
                        if (genres.innerHTML.indexOf(genre_filter_list[j]) < 0) {
                            domClass.add(tr, 'blur');
                            count--;
                            filtered = true;
                            break;
                        }
                    }
                    if (filtered) {
                        continue;
                    }
                }
            }

            if (untag_filter_list.length) {
                group_map = {};
                query('.tag', tr.cells[1]).forEach(function(tag) {
                    var pieces = tag.getAttribute('data-tag-name').split(' / ');
                    if (pieces.length > 1) {
                        group_map[pieces[0]] = true;
                    }
                });
                filtered = false;
                for (var j = 0, l = untag_filter_list.length; j < l; ++j) {
                    if (group_map[untag_filter_list[j]]) {
                        domClass.add(tr, 'blur');
                        count--;
                        filtered = true;
                        break;
                    }
                }
                if (filtered) {
                    continue;
                }
            }

            if (tag_filter_list.length) {
                tag_map = {};
                query('.tag', tr.cells[1]).forEach(function(tag) {
                    tag_map[tag.getAttribute('data-tag-name')] = true;
                });
                for (var j = 0, l = tag_filter_list.length; j < l; ++j) {
                    if (!tag_map[tag_filter_list[j]]) {
                        domClass.add(tr, 'blur');
                        count--;
                        break;
                    }
                }
            }
        }

        if (!count) {
            domClass.add(table.tBodies[0], 'hidden');
            domClass.remove(table.tBodies[1], 'hidden');
        } else {
            domClass.remove(table.tBodies[0], 'hidden');
            domClass.add(table.tBodies[1], 'hidden');
        }
    }; 

    //--------------------------------------------------------------------------
    var setup_filtering = function() {
        /**
         * Provide the functionality for the filter interface.
        **/
        var remove_filter_click_handler = function(evt) {
            /**
             * Invoked when a filter is removed.
            **/
            event.stop(evt);

            domClass.add(evt.currentTarget.parentNode, 'hidden');

            _filter_library();
            tabulate();
            _fix_table_stripes();
        };
        on(query('.filter-list >.filter >a', 'filter-list'), 'click', remove_filter_click_handler);

        var filter_click_handler = function(evt) {
            /**
             * Clicking on a filter should filter the library display.
            **/
            event.stop(evt);

            var filter_list = query('.filter-list >.filter', 'filter-list');

            var tag_name = evt.currentTarget.getAttribute('data-tag-name');
            if (tag_name) {
                filter_list.some(function(filter) {
                    if (filter.getAttribute('data-tag-name') == tag_name) {
                        domClass.remove(filter, 'hidden');
                        return true;
                    }
                });
            } 

            var untag_name = evt.currentTarget.getAttribute('data-untag-name');
            if (untag_name) {
                filter_list.some(function(filter) {
                    if (filter.getAttribute('data-untag-name') == untag_name) {
                        domClass.remove(filter, 'hidden');
                        return true;
                    }
                });
            }


            var genre = evt.currentTarget.getAttribute('data-genre');
            if (genre) {
                filter_list.some(function(filter) {
                    if (filter.getAttribute('data-genre') == genre) {
                        domClass.remove(filter, 'hidden');
                        return true;
                    }
                });
            }

            var rating = evt.currentTarget.getAttribute('data-rating');
            if (rating) {
                filter_list.some(function(filter) {
                    if (filter.getAttribute('data-rating') == rating) {
                        domClass.remove(filter, 'hidden');
                        return true;
                    }
                });
            }

            var is_finished = evt.currentTarget.getAttribute('data-finished');
            if (is_finished) {
                // Special case: Only one of Yes or No are allowed for the Finished filter.
                filter_list.forEach(function(filter) {
                    if (filter.getAttribute('data-finished')) {
                        if (filter.getAttribute('data-finished') == is_finished) {
                            domClass.remove(filter, 'hidden');
                        } else {
                            domClass.add(filter, 'hidden');
                        }
                    }
                });
            }

            var hours = evt.currentTarget.getAttribute('data-hours');
            if (hours) {
                filter_list.some(function(filter) {
                    if (filter.getAttribute('data-hours') == hours) {
                        domClass.remove(filter, 'hidden');
                        return true;
                    }
                }); 
            } 

            var cost = evt.currentTarget.getAttribute('data-cost');
            if (cost) {
                filter_list.some(function(filter) {
                    if (filter.getAttribute('data-cost') == cost) {
                        domClass.remove(filter, 'hidden');
                        return true;
                    }
                });
            }

            _filter_library();
            tabulate();
            _fix_table_stripes();
        };
        on(query('.tag-list .tag', table), 'click', filter_click_handler);
        on(query('.dropdown a', 'filter-column-header'), 'click', filter_click_handler);
    };

    //--------------------------------------------------------------------------
    var pre_filter_library = function() {
        /** 
         * Filter the Library down initially based on GET parameters given in the URL.
        **/
        var apply_filtering = false;
        var apply_sorting = false;
        var filters = query('.filter-list >.filter', 'filter-list');
        var params = extract_hash_parameters(window.location.hash);

        if (params['tag']) {
            array.forEach(filters, function(filter) {
                if (array.indexOf(params['tag'], filter.getAttribute('data-tag-name')) > -1) {
                    domClass.remove(filter, 'hidden');
                    apply_filtering = true;
                }
                if (array.indexOf(params['tag'], filter.getAttribute('data-untag-name')) > -1) {
                    domClass.remove(filter, 'hidden');
                    apply_filtering = true;
                }
            });
        }

        if (params['genre']) {
            // Pre-filter the Library by genre.
            array.forEach(filters, function(filter) {
                if (array.indexOf(params['genre'], filter.getAttribute('data-genre')) > -1) {
                    domClass.remove(filter, 'hidden');
                    apply_filtering = true;
                }
            });
        }

        if (params['hours']) {
            // Pre-filter the Library by hours.
            array.forEach(filters, function(filter) {
                if (array.indexOf(params['hours'], filter.getAttribute('data-hours')) > -1) {
                    domClass.remove(filter, 'hidden');
                    apply_filtering = true;
                }
            }); 
        }

        if (params['cost']) {
            // Pre-filter the Library by cost.
            array.forEach(filters, function(filter) {
                if (array.indexOf(params['cost'], filter.getAttribute('data-cost')) > -1) {
                    domClass.remove(filter, 'hidden');
                    apply_filtering = true;
                }
            });
        }

        if (params['enjoyment']) {
            // Pre-filter the Library by enjoyment rating.
            array.forEach(filters, function(filter) {
                if (array.indexOf(params['enjoyment'], filter.getAttribute('data-rating')) > -1) {
                    domClass.remove(filter, 'hidden');
                    apply_filtering = true;
                }
            });
        }

        if (params['finished']) {
            array.forEach(filters, function(filter) {
                if (array.indexOf(params['finished'], filter.getAttribute('data-finished')) > -1) {
                    domClass.remove(filter, 'hidden');
                    apply_filtering = true;
                }
            });
        }
        if (apply_filtering) {
            // We don't tabulate here because it is done later in init();
            _filter_library();
        }

        if (params['sort']) {
            var column = dom.byId(params['sort'][0] + '-column-header');
            if (column) {
                var anchor = query('>a', column)[0];
                var direction = params['sort'][1];

                if (direction == 'ASC') {
                    domClass.add(anchor, 'sort-descending');
                    domClass.remove(anchor, 'sort-ascending');
                } else {
                    domClass.add(anchor, 'sort-ascending');
                    domClass.remove(anchor, 'sort-descending'); 
                }
                apply_sorting = true;
                on.emit(
                    anchor, 'click', {
                        bubbles: true,
                        cancelable: true 
                    }
                );
            }
        }

        if (apply_filtering && !apply_sorting) {
            // If we don't set an initial table sort, then we need to fix the table stripes.
            _fix_table_stripes();
        }
    };

    //-------------------------------------------------------------------------- 
    var setup_friends = function() {
        var select = query('.friends select')[0];
        select.selectedIndex = 0;
        on(select, 'change', function(evt) {
            if (select.selectedIndex > 3) {
                window.location = '/member/' + select[select.selectedIndex].value + '/games/';
            } else if (select.selectedIndex == 2) {
                window.location = '/profile/games/';
            }
        });
    };

    //--------------------------------------------------------------------------
    var init = function() {
        setup_table_sorting(); 
        setup_filtering();
        setup_friends();

        pre_filter_library();
        tabulate();
    };
    return init;
});
