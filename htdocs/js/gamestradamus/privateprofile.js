define([
    "dojo/on",
    "dojo/_base/event",
    "dojo/dom",
    "dojo/dom-class",
    "atemi/io/jsonrpc",
    "dojo/domReady!"
], function(on, event, dom, domClass, jsonrpc) {

    var init = function() {
        var rpc = jsonrpc('/jsonrpc');
        var now = (new Date()).getTime();
        var timer;
        var are_games_updated = function() {
            rpc.request({
                method: 'predictions.last-game-library-update',
                params: []
            }, function(data) {
                var private_profile_since = Date.parse(data.private_profile_since);
                var last_update = Date.parse(data.last_updated);
                if (!isNaN(private_profile_since) && private_profile_since > now) {
                    clearInterval(timer);
                    domClass.add(dom.byId('update-games-loader'), 'hidden');
                    domClass.remove(dom.byId('still-private'), 'hidden');
                } else if (!isNaN(last_update) && last_update > now) {
                    clearInterval(timer);
                    window.location.reload();
                }
            });
        };

        // If there is a prophesy call running when the page is loaded, 
        // wait until the operation is finished and reload the page.
        var update_games_loader = dom.byId('update-games-loader');
        if (!domClass.contains(update_games_loader, 'hidden')) {
            timer = setInterval(are_games_updated, 1500);
        }

        var update_predictions = dom.byId('update-games');
        if (update_predictions) {
            on(update_predictions, 'click', function(evt) {
                event.stop(evt);

                domClass.remove(dom.byId('update-games-loader'), 'hidden');
                domClass.add(dom.byId('still-private'), 'hidden');

                rpc.request({
                    method: 'predictions.update-games',
                    params: []
                }, function(data) {
                    timer = setInterval(are_games_updated, 1500); 
                });
            }); 
        }
    };
    return init;
});
