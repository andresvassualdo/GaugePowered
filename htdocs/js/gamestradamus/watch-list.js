define([
    "atemi/io/jsonrpc",
    "atemi/util/TableSort",

    "gamestradamus/add-email",
    "gamestradamus/region",

    "dojo/_base/array",
    "dojo/_base/event",
    "dojo/_base/fx",
    "dojo/_base/lang",
    "dojo/on",
    "dojo/query",
    "dojo/dom",
    "dojo/dom-class",
    "dojo/dom-construct",
    "bootstrap/Tooltip",
    "dojo/NodeList",
    "dojo/NodeList-traverse"
], function(
    jsonrpc, TableSort,
    AddEmail, region,
    array, event, fx, lang, on, query, dom, domClass, domConstruct, Tooltip
) {

    var init = function() {
        var rpc = jsonrpc('/jsonrpc');
        var table = dom.byId('watch-list');
        var watches = query('tbody tr.watch', table);

        array.forEach(watches, function(watch) {

            var on_remove_watch = function(evt) {
                event.stop(evt);
                var watch = query(evt.currentTarget).parents('tr')[0];
                var watch_id = parseInt(watch.getAttribute("data-watch-id"));
                var game_id = parseInt(watch.getAttribute("data-game-id"));
                rpc.request({
                    method: 'useraccountgamewatch.delete',
                    params: ["id=" + watch_id]
                }, function(data) {
                    fx.animateProperty({
                        node: watch,
                        properties: {
                            opacity: {start: 1, end: 0}
                        },
                        onEnd: function() {
                            watch.parentNode.removeChild(watch);

                            var watches = query('tbody tr.watch', table);
                            var superscript = 1; 
                            var last_watch = null;
                            array.forEach(watches, function(watch, i) {
                                // Fix the tab index ordering for the rows.
                                var limit = query('input.limit', watch)[0];
                                var operation = query('select.operation', watch)[0];
                                limit.tabIndex = i + 1;
                                operation.tabIndex = i + watches.length + 1;

                                // Fix the position counter for the rows.
                                watch.cells[0].innerHTML = (i + 1);

                                // Fix the superscripts for duplicate game watches.
                                if (parseInt(watch.getAttribute('data-game-id')) == game_id) {
                                    last_watch = watch;

                                    var sup = query('h4 sup', watch)[0];
                                    sup.innerHTML = '[' + superscript + ']';
                                    superscript++;
                                }
                            });
                            if (superscript == 2) {
                                // In the event that there is only one watch, remove the superscript.
                                var sup = query('h4 sup', last_watch)[0];
                                sup.innerHTML = '';
                            }

                            if (watches.length < 1) {
                                // show the empty watchlist message.
                                domClass.add(dom.byId('watch-list'), 'hidden');
                                domClass.remove(dom.byId('empty-watch-list'), 'hidden');
                                domClass.add(dom.byId('add-email'), 'hidden');
                            }
                        }
                    }).play();
                });
            };

            var on_copy_watch = function(evt) {
                event.stop(evt);
                
                var watch = query(evt.currentTarget).parents('tr')[0];
                var watch_id = parseInt(watch.getAttribute("data-watch-id"));
                var game_id = parseInt(watch.getAttribute("data-game-id"));
                rpc.request({
                    method: 'useraccountgamewatch.create',
                    params: ['', {
                        game_id: game_id
                    }]
                }, function(data) {
                    var new_watch = lang.clone(watch);
                    new_watch.setAttribute('data-watch-id', data[0].id);

                    var watches = query('tr.watch', table);
                    var last_watch = null;
                    var superscript = 1;
                    array.forEach(watches, function(watch) {
                        if (parseInt(watch.getAttribute('data-game-id')) == game_id) {
                            last_watch = watch;
                            superscript++;
                        }
                    });
                   
                    // Fix the superscripts.
                    var sup = query('h4 sup', new_watch)[0];
                    sup.innerHTML = '[' + superscript + ']';
                    sup = query('h4 sup', last_watch)[0];
                    sup.innerHTML = '[' + (superscript - 1) + ']';

                    // Reset the notification inputs.
                    var operation = query('select.operation', new_watch)[0];
                    operation.selectedIndex = 0;
                    var limit = query('input.limit', new_watch)[0];
                    limit.value = ''; 

                    /* Hook into the remove links */
                    var link = query('.remove-watch', new_watch)[0];
                    on(link, 'click', on_remove_watch);

                    /* Hook into the copy links */
                    var copy = query('.copy-watch', new_watch)[0];
                    on(copy, 'click', on_copy_watch);

                    /* Hook into the notify changes */
                    var limit = query('.limit', new_watch)[0];
                    var operation = query('.operation', new_watch)[0];
                    on(limit, 'change', on_notify_change);
                    on(operation, 'change', on_notify_change);

                    // remove any highlights
                    domClass.remove(new_watch, 'satisfied');
                    domClass.remove(query('>td.cost-per-hour', new_watch)[0], 'active');
                    domClass.remove(query('>td.cost-per-hour', new_watch)[0], 'inactive');
                    domClass.remove(query('>td.price', new_watch)[0], 'active');
                    domClass.remove(query('>td.price', new_watch)[0], 'inactive');

                    // Put the new row at the end of the game groups.
                    domConstruct.place(new_watch, last_watch, 'after');

                    var watches = query('tr.watch', table);
                    array.forEach(watches, function(watch, i) {

                        // Fix the tab index ordering for the rows.
                        var limit = query('input.limit', watch)[0];
                        var operation = query('select.operation', watch)[0];
                        limit.tabIndex = i + 1;
                        operation.tabIndex = i + watches.length + 1;

                        // Fix the row position counter.
                        watch.cells[0].innerHTML = (i + 1);
                    });

                    // animate the new watch into view.
                    fx.animateProperty({
                        node: new_watch,
                        properties: {
                            opacity: {start: 0.01, end: 1}
                        }
                    }).play();
                }); 
            };

            var on_notify_change = function(evt) {
                event.stop(evt);

                var watch = query(evt.currentTarget).parents('tr')[0];
                var watch_id = parseInt(watch.getAttribute("data-watch-id"));
                var limit = query('.limit', watch)[0];
                var operation = query('.operation', watch)[0];

                value = region['parse-dollars'](limit.value);
                if (isNaN(value)) {
                    value = null;
                } else {
                    if (value == 0) {
                        value = null;
                    } else {
                        if (limit.value.slice(-1) == '%') {
                            if (value > 99 || value < -99) {
                                return;
                            }
                            if (value > 0) {
                                if (operation.value == 'price') {
                                    value = (value * region['parse-dollars'](watch.getAttribute('data-base-price'))) / 100;
                                } else {
                                    value = (value * region['parse-dollars'](watch.getAttribute('data-base-cost-per-hour'))) / 100;
                                }
                            } else {
                                if (operation.value == 'price') {
                                    value = ((100 + value) * region['parse-dollars'](watch.getAttribute('data-base-price'))) / 100;
                                } else {
                                    value = ((100 + value) * region['parse-dollars'](watch.getAttribute('data-base-cost-per-hour'))) / 100;
                                }
                            }
                        }
                        value = value.toFixed(2);
                    }
                } 

                rpc.request({
                    method: 'useraccountgamewatch.update',
                    params: [
                        "id=" + watch_id, {
                            limit: value,
                            operation: operation.value
                        }
                    ]
                }, function(data) {;
                    limit.blur();
                    domClass.remove(watch, 'satisfied');
                    domClass.remove(query('>td.cost-per-hour', watch)[0], 'active');
                    domClass.remove(query('>td.cost-per-hour', watch)[0], 'inactive');
                    domClass.remove(query('>td.price', watch)[0], 'active');
                    domClass.remove(query('>td.price', watch)[0], 'inactive');

                    if (data[0].operation == 'price') {
                        if (data[0].limit == null) {
                            limit.value = '';
                        } else {
                            limit.value = region['format-dollars'](data[0].limit).replace('&nbsp;', ' ');
                        }
                        var watch_price = parseFloat(data[0].limit);
                        if (!isNaN(watch_price)) {
                            domClass.add(query('>td.price', watch)[0], 'active');
                            domClass.add(query('>td.cost-per-hour', watch)[0], 'inactive');
                            var game_price = parseFloat(watch.getAttribute('data-price'));
                            if (game_price <= watch_price) {
                                domClass.add(watch, 'satisfied');
                            }
                        }
                    }

                    if (data[0].operation == 'cost-per-hour') {
                        if (data[0].limit == null) {
                            limit.value = '';
                        } else {
                            limit.value = region['format-dollars'](data[0].limit).replace('&nbsp;', ' ');
                        }
                        var watch_cost_per_hour = parseFloat(data[0].limit);
                        if (!isNaN(watch_cost_per_hour)) {
                            domClass.add(query('>td.cost-per-hour', watch)[0], 'active');
                            domClass.add(query('>td.price', watch)[0], 'inactive');
                            var game_cost_per_hour = parseFloat(watch.getAttribute('data-cost-per-hour'));
                            if (game_cost_per_hour <= watch_cost_per_hour) {
                                domClass.add(watch, 'satisfied');
                            }
                        }
                    }
                });
            };

            /* Hook into the remove links */
            var link = query('.remove-watch', watch)[0];
            on(link, 'click', on_remove_watch);

            /* Hook into the copy links */
            var copy = query('.copy-watch', watch)[0];
            on(copy, 'click', on_copy_watch);

            /* Hook into the notify changes */
            var limit = query('.limit', watch)[0];
            var operation = query('.operation', watch)[0];
            on(limit, 'change', on_notify_change);
            on(operation, 'change', on_notify_change);

        });

        /* Setup the table sort */
        new TableSort(
            table, [0, 0], [
                TableSort.parseInt,
                TableSort.textContent,
                function(item) { return parseFloat(item.getAttribute('data-rating')); },
                function(item) { return parseFloat(item.getAttribute('data-hours')); },
                function(item) { return parseFloat(item.getAttribute('data-price')); },
                function(item) { return parseFloat(item.getAttribute('data-cost-per-hour')); },
                function(item) {
                    var operation = query('select.operation', item)[0];
                    var limit = query('input.limit', item)[0];

                    // Reverse the operation string so that it is sorted in reverse.
                    operation = operation.value;
                    operation = operation.split('').reverse().join('');

                    // Default the limit value to zero when not present.
                    limit = region['parse-dollars'](limit.value);
                    if (isNaN(limit)) {
                        limit = 0;
                    }
                    return [operation, limit];
                }
            ],
            function(data) {
                // Fix the tab index ordering for the rows.
                array.forEach(data, function(row, i) {
                    var limit = query('input.limit', row[0])[0];
                    var operation = query('select.operation', row[0])[0];
                    limit.tabIndex = i + 1;
                    operation.tabIndex = i + data.length + 1;
                });

                // Fix the position counter for the rows.
                array.forEach(data, function(row, i) {
                    row[0].cells[0].innerHTML = '<div>' + (i + 1) + '</div>';
                });
            }
        ); 


        //---------------------------------------------------------------------
        // Get the quick add email button working
        var add_email = new AddEmail({
            subscribe_button_selector: "#subscribe",
            email_input_selector: "#email",
            form_selector: "#marketing-form",
            marketing_input_selector: "#marketing",
            success_handler: function(data) {
                fx.animateProperty({
                    node: dom.byId('add-email'),
                    properties: {opacity: 1, opacity: 0},
                    onEnd: function() {
                        domClass.add(dom.byId('add-email'), 'hidden');
                        domClass.remove(dom.byId('add-email-success'), 'hidden');
                    }
                }).play(); 
            }
        });

        /* Hook up to the Data usage opt out */
        var watchlist_email_ignore = query("#ignore-email");
        watchlist_email_ignore.on('click', function(evt) {
            event.stop(evt);

            var id = parseInt(watchlist_email_ignore[0].getAttribute("data-tag-id"));
            rpc.request({
                method: 'useraccountattributes.update',
                params: ['id=' + id + '&select(id,has_ignored_watchlist_email)', {
                    has_ignored_watchlist_email: true
                }] 
            }, function(data) {
                if (data[0].has_ignored_watchlist_email) {
                    fx.animateProperty({
                        node: dom.byId('add-email'),
                        properties: {opacity: 1, opacity: 0},
                        onEnd: function() {
                            domClass.add(dom.byId('add-email'), 'hidden');
                            domClass.remove(dom.byId('add-email-ignored'), 'hidden');
                        }
                    }).play(); 
                }
            });
        });

        on(query('a.dismiss'), 'click', function(evt) {
            event.stop(evt);

            var node = evt.currentTarget.parentNode;
            fx.animateProperty({
                node: node,
                properties: {opacity: 1, opacity: 0},
                onEnd: function() {
                    domClass.add(node, 'hidden');
                }
            }).play(); 
        });
    };
    return init;
});
