// Configure library packages to work with AMD loader
var dojoConfig = {
    async: true,
    basePath:"./",
    baseUrl:"/js",
    selectorEngine: "acme",
    //deferredOnError: function(error){} /* Ignore deferred errors generally */,
 
    packages:[{
        name: "dojo",
        location: "dojo"
    },{
        name: "atemi",
        location: "atemi"
    },{
        name: "bootstrap",
        location: "bootstrap"
    },{
        name: 'gamestradamus',
        location: 'gamestradamus'
    }]
};
