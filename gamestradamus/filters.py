"""
The gamestradamus filters
"""
import cherrypy
import urllib
import toro

from gamestradamus import timeutils
from jinja2 import evalcontextfilter, Markup, escape

#-------------------------------------------------------------------------------
def ago(dt):
    if dt:
        d = timeutils.utcnow() - dt
        seconds = (d.microseconds + (d.seconds + d.days * 24 * 3600) * 10**6) / 10**6
        if seconds < 60:
            return '%s secs ago' % seconds
        elif seconds < 3600:
            return '%s mins ago' % (seconds / 60)
        elif seconds < 86400:
            return '%s hrs ago' % (seconds / 3600)
        elif seconds < 172800:
            return 'Yesterday'
        else:
            return '%s days ago' % (seconds / 86400)
    else:
        return 'Unknown'

#-------------------------------------------------------------------------------
@evalcontextfilter
def thousands_separated(eval_ctx, num):
    """Put commas into a number."""
    separator = ","
    radix = "."
    region = getattr(cherrypy.request, 'user_region', None)
    if region:
        separator = region['numbers'].thousands_separator
        radix = region['numbers'].radix

    num = u'%s' % num
    pieces = num.split(radix)

    l = []
    x = len(pieces[0])
    while x > 0:
        s = slice(max(0, x-3), x)
        l.insert(0, pieces[0][s])
        x = x - 3

    pieces[0] = separator.join(l)
    result = radix.join(pieces)
    if eval_ctx.autoescape:
        result = Markup(result)
    return result

#-------------------------------------------------------------------------------
def urlencode(param):
    """Encode a parameter appropriate for inclusion on a url."""

    param = urllib.urlencode({'': param})
    return param[1:]

#-------------------------------------------------------------------------------
def tag_name(qualified_name):
    """Return the unqualified name for a tag."""

    pieces = qualified_name.split(' / ')
    if len(pieces) > 1:
        return pieces[1]
    return pieces[0]

#-------------------------------------------------------------------------------
def tag_group(qualified_name):
    """Return the group for a tag."""

    pieces = qualified_name.split(' / ')
    if len(pieces) > 1:
        return pieces[0]
    return None

#-------------------------------------------------------------------------------
filters = {
    'ago': ago,
    'thousands_separated': thousands_separated,
    'urlencode': urlencode,
    'tag_name': tag_name,
    'tag_group': tag_group,
}
filters.update(toro.filters)
