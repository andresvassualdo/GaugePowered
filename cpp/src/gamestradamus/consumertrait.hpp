//------------------------------------------------------------------------------
// A consumer trait is a measurable sortable trait of a consumer that we can use
// to provide recommendations.
//------------------------------------------------------------------------------
#ifndef GAMESTRADAMUS_CONSUMER_TRAIT_HPP
#define GAMESTRADAMUS_CONSUMER_TRAIT_HPP

#include <gamestradamus/consumer.hpp> 

namespace gamestradamus {

template <class Trait>
class ConsumerTrait {
public:
    //--------------------------------------------------------------------------
    ConsumerTrait() { }

    //--------------------------------------------------------------------------
    virtual Trait get_trait() = 0

};// end class Account

}//end namespace gamestradamus

#endif//GAMESTRADAMUS_CONSUMER_TRAIT_HPP

